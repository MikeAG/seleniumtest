import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PortalTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
// Create a new instance of the chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lenovo\\Desktop\\2011\\second semester\\SE\\selenium\\chrome driver\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
//Launch the some site. in this case AAIT Portal
        driver.get("https://portal.aau.edu.et/");

        //get input and button fields from the portal
        WebElement email = driver.findElement(By.id("UserName"));
        WebElement password = driver.findElement(By.name("Password"));
        WebElement login = driver.findElement(By.id("home")).findElement(By.className("btn"));

        // after input, action is needed to trigger the process
        email.sendKeys("ATR/1234/09"); // Real ID is not used for security reasons.
        password.sendKeys("1234");     // Real Password is not used for security reasons.
        login.click();

        //after a successful log in, the page forwards to the grade report page
        driver.navigate().to("https://portal.aait.edu.et/Grade/GradeReport");

        //grade report is accessed using the table elements
        String gradeReportTableData = driver.findElement(By.tagName("table")).getText();

        Thread.sleep(2000);


        // write the table retrieved to a text file
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter("GradeReport.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.append(gradeReportTableData);
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


// Close the driver
        driver.close();


    }
}