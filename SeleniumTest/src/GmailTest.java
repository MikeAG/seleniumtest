import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class GmailTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
// Create a new instance of the chrome driver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lenovo\\Desktop\\2011\\second semester\\SE\\selenium\\chrome driver\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
//Launch the some site, in this case gmail
        driver.get("https://mail.google.com/mail/u/0/#inbox");

        driver.findElement(By.id("identifierId")).sendKeys("seleniumtestgmail@gmail.com");
        driver.findElement(By.id("identifierNext")).click();

        //giving it time to check the email entered
        Thread.sleep(10000L);

        driver.findElement(By.name("password")).sendKeys("abcd1234@abc");
        driver.findElement(By.id("passwordNext")).click();
        //give it time to validate the password
        Thread.sleep(40000L);


        List<WebElement> mailsObj=driver.findElements(By.xpath("//*[@class='zA zE']"));
        System.out.println("The total number of unread mails are=== "+mailsObj.size());
        mailsObj.get(0).click();

        FileWriter filewriter= null;
        try {
            filewriter = new FileWriter("grade.txt");
            for (WebElement message: mailsObj  ) {
                System.out.println(message.getText());
                filewriter.write(message.getText()+"\n");

            }
            filewriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }



// Close the driver
        driver.quit();


    }
}
